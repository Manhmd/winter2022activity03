public class Shoe {
  public String brand;
  public double size;
  public String color;
  
  public void describeShoes(){
    System.out.println("These are "+ this.color +", "+ this.brand +" brand shoes that have a size of "+ this.size +".");
  }
}