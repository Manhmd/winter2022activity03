import java.util.Scanner;

public class Shop {
  public static void main(String args[]){
    Shoe[] shoeShelf = new Shoe[4];
    
    //Create Loop to get input from user for each pair of shoes in the shoe shelf
    for (int i = 0; i < shoeShelf.length; i++){
      int number = i+1;
      
      //Get the values with a scanner
      Scanner keyboard = new Scanner(System.in);
      System.out.print("Enter the brand for shoe #"+ number +": ");
      String brand= keyboard.nextLine();
      System.out.print("Enter the color for shoe #"+ number +": ");
      String color = keyboard.nextLine();
      System.out.print("Enter the size for shoe #"+ number +": ");
      double size = keyboard.nextDouble();
      
      // Store the values in the array
      shoeShelf[i] = new Shoe();
      shoeShelf[i].brand = brand;
      shoeShelf[i].size = size;
      shoeShelf[i].color = color;
    }
    //Print the characteristics of the last pair of shoes on the shelf
    System.out.println(shoeShelf[3].brand +", "+ shoeShelf[3].size +", "+ shoeShelf[3].color +".");
    
    //Describe the shoes 
    shoeShelf[3].describeShoes();
  }
}